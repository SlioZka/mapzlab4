﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab4
{ 
    class TaskInfo
    {
        public string name { get; set; }
        public int day { get; set; }
        public TaskInfo(string _name, int _day)
        {
            name = _name;
            day = _day;
        }
        public void GetInfo()
        {
            Console.WriteLine(name);
            int temp = 15 - day;
            if (temp > 0)
            {
                Console.WriteLine("SADGE!!! The Task ended " + temp + " days ago\n");
            }
            else
            {
                Console.WriteLine("HURRAAAY!!! You have " + (-temp) + " days for the task\n");
            }
        }
    }
    abstract class TaskDecorator: TaskInfo
    {
        protected TaskInfo taskInfo;
        public TaskDecorator(string _name, int _day, TaskInfo _taskInfo) 
            : base (_name, _day)
        {
            taskInfo = _taskInfo;
        }
    }
    class ImportantTask : TaskDecorator
    {
        public ImportantTask(TaskInfo task)
            : base ("*!!!*" + task.name + "*!!!*", task.day, task)
        { }
    }
    class DeadTask : TaskDecorator
    {
        public DeadTask(TaskInfo task)
            : base ("#x_x#" + task.name + "#x_x#", task.day, task)
        { }
    }
}
