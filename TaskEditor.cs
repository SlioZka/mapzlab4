﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab4
{
    class TaskEditor
    {
        public string author { get; set; }
        public char autorize { get; set; }
        public TaskEditor(string _author, char _autorize)
        {
            author = _author;
            autorize = _autorize;
        }
        public void ShowAuthor()
        {
            if (autorize == 'a')
                Console.WriteLine("Created by admin: " + author);
            else
                Console.WriteLine("Created by: " + author);
        }
    }
    class Reminder
    {
        public string title { get; set; }
        public string date { get; set; }
        public string type { get; set; }
        public Reminder(string _title, string _date, string _type)
        {
            title = _title;
            date = _date;
            type = _type;
            //Console.WriteLine("Title: {0}\nDate: {1}", title, date);
        }
        public void ShowInfo()
        {
            if (type == "task")
                Console.WriteLine("|New Task|");
            else if (type == "reminder")
                Console.WriteLine("|New Reminder|");
            else
                Console.WriteLine("|New|");
            Console.WriteLine("Title: {0}\nDate: {1}", title, date);
        }
    }
    class TaskEditorFacade
    {
        public Reminder task { get; set; }
        public TaskEditor taskEditor { get; set; }
        public TaskEditorFacade(Reminder _task, TaskEditor _taskEditor)
        {
            task = _task;
            taskEditor = _taskEditor;
        }
        public void Start()
        {
            task.ShowInfo();
            taskEditor.ShowAuthor();
        }
    }
}
