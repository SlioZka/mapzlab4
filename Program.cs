﻿using System;

namespace MAPZLab4
{
    class Program
    {
        static void Main(string[] args)
        {
            //FACADE
            //Reminder task = new Reminder("Buy milk", "23.05.21", "reminder");
            //Reminder task1 = new Reminder("Call a doctor", "21.05.21", "task");
            //TaskEditor taskEditor = new TaskEditor("Serhii", 'u');
            //TaskEditor taskEditor1 = new TaskEditor("Anonymous", 'a');
            //TaskEditorFacade facade = new TaskEditorFacade(task, taskEditor);
            //TaskEditorFacade facade1 = new TaskEditorFacade(task1, taskEditor1);
            //facade.Start();
            //facade1.Start();

            //COMPONENT
            //Calendar cale = new Month("March");
            //Calendar task1 = new Info("Happy birthday");
            //Calendar task2 = new Info("Play 3 LoL`s games");
            //cale.Add(task1);
            //cale.Add(task2);
            //cale.Print();
            //cale.Remove(task1);
            //cale.Print();

            //DECORATOR
            TaskInfo task1 = new TaskInfo("Buy milk", 18);
            TaskInfo task2 = new TaskInfo("Call Mr.Robbin", 13);
            task1.GetInfo();
            task2.GetInfo();
            task1 = new ImportantTask(task1);
            task2 = new DeadTask(task2);
            task1.GetInfo();
            task2.GetInfo();

        }
    }
}
