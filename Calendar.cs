﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab4
{
    abstract class Calendar
    {
        protected string name;
        public Calendar(string _name)
        {
            name = _name;
        }
        public virtual void Add(Calendar title) { }

        public virtual void Remove(Calendar title) { }

        public virtual void Print()
        {
            Console.WriteLine("\t" + name);
        }
    }
    class Month : Calendar
    {
        public List<Calendar> tasks = new List<Calendar>();
        public Month(string name) : base(name)
        { }
        public override void Add(Calendar component)
        {
            tasks.Add(component);
        }
        public override void Remove(Calendar component)
        {
            tasks.Remove(component);
        }
        public override void Print()
        {
            Console.WriteLine("MONTH: " + name);
            for (int i = 0; i < tasks.Count; i++)
            {
                tasks[i].Print();
            }
        }
    }
    class Info : Calendar
    {
        public Info(string name) : base(name)
        { }
    }
}
